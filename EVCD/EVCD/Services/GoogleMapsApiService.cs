﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
//using EVCD.Models;
using Xamarin.Forms.GoogleMaps;
using static SQLite.SQLite3;

namespace EVCD.Services
{
    public class GoogleMapsApiService : IGoogleMapsApiService
    {
        static string _googleMapsKey;

        private const string ApiBaseAddress = "https://maps.googleapis.com/maps/";
        private const string NRELApiBaseAddress = "https://developer.nrel.gov/";

        private HttpClient CreateClient()
        {
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri(ApiBaseAddress)
            };

            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return httpClient;
        }

        private HttpClient CreateClientNREL()
        {
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri(NRELApiBaseAddress)
            };

            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return httpClient;
        }

        //private HttpClient client;
        public static void Initialize(string googleMapsKey)
        {
            _googleMapsKey = googleMapsKey;
            //client = new HttpClient();
            //client.BaseAddress = new Uri("https://maps.googleapis.com/maps/");
        }

        //public async Task<GoogleDirection> GetDirections(string originLat, string originLng, string destinationLat, string destinationLng)
        //{
        //    GoogleDirection googleDirection = new GoogleDirection();

        //    using (var httpClient = CreateClient())
        //    {

        //        var response = await httpClient.GetAsync($"api/directions/json?mode=driving&transit_routing_preference=less_driving&origin={originLat},{originLng}" +
        //            $"&destination={destinationLat},{destinationLng}&key={_googleMapsKey}").ConfigureAwait(false);
        //        if (response.IsSuccessStatusCode)
        //        {
        //            var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        //            if (!string.IsNullOrWhiteSpace(json))
        //            {
        //                googleDirection = await Task.Run(() =>
        //                   JsonConvert.DeserializeObject<GoogleDirection>(json)
        //                ).ConfigureAwait(false);

        //            }
        //        }
        //    }

        //    return googleDirection;
        //}

        //public async Task<GoogleDirection> GetDirectionsWaypoints(string originLat, string originLng, string destinationLat, string destinationLng,
        //    string waypointLat, string waypointLng)
        //{
        //    GoogleDirection googleDirectionWaypoint = new GoogleDirection();

        //    using (var httpClient = CreateClient())
        //    {
        //        var response = await httpClient.GetAsync($"api/directions/json?mode=driving&transit_routing_preference=less_driving&origin={originLat}%2C{originLng}" +
        //            $"&destination={destinationLat}%2C{destinationLng}&waypoints={waypointLat}%2C{waypointLng}&key={_googleMapsKey}").ConfigureAwait(false);

        //        //var response = await httpClient.GetAsync($"api/directions/json?mode=driving&transit_routing_preference=less_driving&origin=14.536165%2C120.982243&destination=14.570088%2C121.048138&waypoints=14.529029%2C120.992309&key=AIzaSyBg91fBr4oHHXtw7JZLO3HxDeAlLo65N0c").ConfigureAwait(false);

            
        //        if (response.IsSuccessStatusCode)
        //        {
        //            var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        //            if (!string.IsNullOrWhiteSpace(json))
        //            {
        //                googleDirectionWaypoint = await Task.Run(() =>
        //                   JsonConvert.DeserializeObject<GoogleDirection>(json)
        //                ).ConfigureAwait(false);
        //            }
        //        }
        //    }

        //    return googleDirectionWaypoint;
        //}

        //public async Task<GooglePlaceAutoCompleteResult> GetPlaces(string text)
        //{
        //    GooglePlaceAutoCompleteResult results = null;

        //    using (var httpClient = CreateClient())
        //    {
        //        var response = await httpClient.GetAsync($"api/place/autocomplete/json?input={Uri.EscapeUriString(text)}&key={_googleMapsKey}").ConfigureAwait(false);
        //        if (response.IsSuccessStatusCode)
        //        {
        //            var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        //            if (!string.IsNullOrWhiteSpace(json) && json != "ERROR")
        //            {
        //                results = await Task.Run(() =>
        //                   JsonConvert.DeserializeObject<GooglePlaceAutoCompleteResult>(json)
        //                ).ConfigureAwait(false);

        //            }
        //        }
        //    }

        //    return results;
        //}

        //public async Task<GooglePlace> GetPlaceDetails(string placeId)
        //{
        //    GooglePlace result = null;
        //    using (var httpClient = CreateClient())
        //    {
        //        var response = await httpClient.GetAsync($"api/place/details/json?placeid={Uri.EscapeUriString(placeId)}&key={_googleMapsKey}").ConfigureAwait(false);
        //        if (response.IsSuccessStatusCode)
        //        {
        //            var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        //            if (!string.IsNullOrWhiteSpace(json) && json != "ERROR")
        //            {
        //                result = new GooglePlace(JObject.Parse(json));
        //            }
        //        }
        //    }

        //    return result;
        //}



        //public async Task<GoogleSearchText> GetEVChargingPlacesAsync(string lat, string lng)
        //{
        //    GoogleSearchText result = new GoogleSearchText();

        //    using (var httpClient = CreateClient())
        //    {
        //        var response = await httpClient.GetAsync($"api/place/textsearch/json?location={lat}%2C{lng}" +
        //            $"&query=EV%20charger&radius=10000&key={_googleMapsKey}").ConfigureAwait(false);

        //        if (response.IsSuccessStatusCode)
        //        {
        //            var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        //            if (!string.IsNullOrWhiteSpace(json) && json != "ERROR")
        //            {
        //                result = await Task.Run(() =>
        //                   JsonConvert.DeserializeObject<GoogleSearchText>(json)
        //                );
        //            }
        //        }
        //    }

        //    return result;
        //}

        //public GoogleSearchText GetEVChargingPlaces(string lat, string lng)
        //{
        //    GoogleSearchText result = new GoogleSearchText();

        //    using (var httpClient = CreateClient())
        //    {
        //        var response = httpClient.GetAsync($"api/place/textsearch/json?location={lat}%2C{lng}" +
        //            $"&query=EV%20charging%20stations&radius=50000&key={_googleMapsKey}");

        //        if (response.Result.IsSuccessStatusCode)
        //        {
        //            var json = response.Result.Content.ReadAsStringAsync();
        //            if (!string.IsNullOrWhiteSpace(json.Result) && json.Result != "ERROR")
        //            {
        //                result = JsonConvert.DeserializeObject<GoogleSearchText>(json.Result);
        //            }
        //        }
        //    }

        //    return result;
        //}

        //public GoogleDistanceMatrix GetDistanceMatrix(string originLat, string originLng, string destinationLat, string destinationLng)
        //{
        //    GoogleDistanceMatrix result = null;

        //    using (var httpClient = CreateClient())
        //    {
        //        var response = httpClient.GetAsync($"api/distancematrix/json?destinations={destinationLat},{destinationLng}" +
        //            $"&origins={originLat},{originLng}&units=imperial&key={_googleMapsKey}");

        //        //if (response.IsSuccessStatusCode)
        //        //{
        //        //    var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        //        //    if (!string.IsNullOrWhiteSpace(json) && json != "ERROR")
        //        //    {
        //        //        result = await Task.Run(() =>
        //        //           JsonConvert.DeserializeObject<GoogleDistanceMatrix>(json)
        //        //        ).ConfigureAwait(false);
        //        //    }
        //        //}


        //        //var response = await httpClient.GetAsync($"api/place/details/json?placeid={Uri.EscapeUriString(placeId)}&key={_googleMapsKey}").ConfigureAwait(false);
        //        if (response.Result.IsSuccessStatusCode)
        //        {
        //            var json = response.Result.Content.ReadAsStringAsync();
        //            if (!string.IsNullOrWhiteSpace(json.Result) && json.Result != "ERROR")
        //            {
        //                result = new GoogleDistanceMatrix(JObject.Parse(json.Result));
        //            }
        //        }
        //    }
        //    return result;
        //}

        //public async Task<GoogleDistance> GetDistanceMatrixAsync(string originLat, string originLng, string destinationLat, string destinationLng)
        //{
        //    GoogleDistance result = null;

        //    using (var httpClient = CreateClient())
        //    {
        //        var response = await httpClient.GetAsync($"api/distancematrix/json?destinations={destinationLat},{destinationLng}" +
        //            $"&origins={originLat},{originLng}&units=imperial&key={_googleMapsKey}").ConfigureAwait(false);

        //        if (response.IsSuccessStatusCode)
        //        {
        //            var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        //            if (!string.IsNullOrWhiteSpace(json) && json != "ERROR")
        //            {
        //                result = await Task.Run(() =>
        //                   JsonConvert.DeserializeObject<GoogleDistance>(json)
        //                ).ConfigureAwait(false);
        //            }
        //        }

        //    }
        //    return result;
        //}

        //public async Task<NRELChargers> GetChargerMinnesota()
        //{
        //    NRELChargers result = new NRELChargers();

        //    using (var httpClient = CreateClientNREL())
        //    {
        //        var response = await httpClient.GetAsync($"api/alt-fuel-stations/v1.json?api_key=sLYuc4Vk9Yberq0Vjwo9kJdricWAyJ2TXToWyoRc&fuel_type=ELEC&state=MN").ConfigureAwait(false);

        //        if (response.IsSuccessStatusCode)
        //        {
        //            var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        //            if (!string.IsNullOrWhiteSpace(json) && json != "ERROR")
        //            {
        //                result = await Task.Run(() =>
        //                   JsonConvert.DeserializeObject<NRELChargers>(json)
        //                );
        //            }
        //        }
        //    }

        //    return result;
        //}

        //public async Task<GoogleSearchText> GetParkingMinnesota(double lat, double lng)
        //{
        //    GoogleSearchText result = new GoogleSearchText();

        //    using (var httpClient = CreateClient())
        //    {
        //        var response = await httpClient.GetAsync($"api/place/textsearch/json?location={lat}%2C{lng}" +
        //            $"&query=parking%20lot&radius=200000&key={_googleMapsKey}").ConfigureAwait(false);

        //        if (response.IsSuccessStatusCode)
        //        {
        //            var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        //            if (!string.IsNullOrWhiteSpace(json) && json != "ERROR")
        //            {
        //                result = JsonConvert.DeserializeObject<GoogleSearchText>(json);
        //            }
        //        }
        //    }

        //    return result;
        //}

        //public async Task<GoogleSearchText> GetParkingNearbyMall(double lat, double lng)
        //{
        //    GoogleSearchText result = new GoogleSearchText();

        //    using (var httpClient = CreateClient())
        //    {
        //        var response = await httpClient.GetAsync($"api/place/textsearch/json?location={lat}%2C{lng}" +
        //            $"&query=shopping%20mall&radius=500&key={_googleMapsKey}").ConfigureAwait(false);

        //        if (response.IsSuccessStatusCode)
        //        {
        //            var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        //            if (!string.IsNullOrWhiteSpace(json) && json != "ERROR")
        //            {
        //                result = JsonConvert.DeserializeObject<GoogleSearchText>(json);
        //            }
        //        }
        //    }

        //    return result;
        //}

        //public async Task<List<EVOwners>> GetEvOwnersFromCsv(int year)
        //{
        //    List<EVOwners> evPositionList = null;
        //    List<string> evZipcodeList = new List<string>();
        //    var assembly = typeof(MainPage).GetTypeInfo().Assembly;
        //    string path;
            
        //    switch (year)
        //    {
        //        case 2022:
        //            path = $"EVCD.Models.zip08-19.csv";
        //            break;
        //        case 2021:
        //            path = $"EVCD.Models.zip20.csv";  
        //            break;
        //        case 2020:
        //            path = $"EVCD.Models.zip21.csv";
        //            break;
        //        case 2019:
        //            path = $"EVCD.Models.zip22.csv";
        //            break;
        //        default:
        //            path = $"EVCD.Models.zip08-19.csv";
        //            break;

        //    }

        //    var stream = assembly.GetManifestResourceStream(path);
        //    using (var reader = new StreamReader(stream))
        //    {
        //        while (!reader.EndOfStream)
        //        {
        //            var line = reader.ReadLine();
        //            var values = line.Split(';');

        //            evZipcodeList.Add(values[0]);
        //        }
        //    }
        //    evPositionList = await ConvertZipToPosition(evZipcodeList);
        //    return evPositionList;
        //}

        //public async Task<List<EVOwners>> ConvertZipToPosition(List<string> evZipcodeList)
        //{
        //    List<EVOwners> result = new List<EVOwners>();

        //    using (var httpClient = CreateClient())
        //    {
        //        foreach(var item in evZipcodeList)
        //        {
        //            var response = await httpClient.GetAsync($"api/geocode/json?components=postal_code:{item}&sensor=false&key={_googleMapsKey}").ConfigureAwait(false);

        //            if (response.IsSuccessStatusCode)
        //            {
        //                var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        //                if (!json.Contains("ZERO_RESULTS") && !string.IsNullOrWhiteSpace(json) && json != "ERROR")
        //                {
        //                    result.Add(new EVOwners(JObject.Parse(json)));

        //                }
        //            }
        //        }
        //    }
        //    return result;
        //}

        //public async Task<bool> filterMinnesota(double lat, double lng)
        //{
        //   // List<EVOwners> result = new List<EVOwners>();

        //    using (var httpClient = CreateClient())
        //    {

        //            var response = await httpClient.GetAsync($"api/geocode/json?latlng={lat},{lng}&sensor=false&key={_googleMapsKey}").ConfigureAwait(false);

        //            if (response.IsSuccessStatusCode)
        //            {
        //                var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        //                if (json.Contains("Minnesota"))
        //                {
        //                    //result.Add(new EVOwners(JObject.Parse(json)));
        //                    return true;
        //                }
        //            }
        //            else
        //            {
        //                return false;
        //            }
        //    }
        //    return false;
        //}

        //public async Task<GoogleSearchText> CheckNearbyCharger(double lat, double lng)
        //{
        //    GoogleSearchText result = new GoogleSearchText();

        //    using (var httpClient = CreateClient())
        //    {
        //        var response = await httpClient.GetAsync($"api/place/textsearch/json?location={lat}%2C{lng}" +
        //            $"&query=electric%20vehicle%20charging%20stations&radius=500&key={_googleMapsKey}").ConfigureAwait(false);

        //        if (response.IsSuccessStatusCode)
        //        {
        //            var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        //            if (!json.Contains("ZERO_RESULTS") && !string.IsNullOrWhiteSpace(json))
        //            {
        //                result = JsonConvert.DeserializeObject<GoogleSearchText>(json);
        //            }   
        //        }
        //    }
        //    return result;
        //}

        //public async Task<bool> removeParkingNearExistingEV(double parkLat, double parkLng, double evLat, double evLng)
        //{
        //    DistanceToExistingEv result = null;
        //    bool isNearExistingEV = false;

        //    using (var httpClient = CreateClient())
        //    {
        //        var response = await httpClient.GetAsync($"api/distancematrix/json?destinations={parkLat},{parkLng}" +
        //            $"&origins={evLat},{evLng}&units=imperial&key={_googleMapsKey}").ConfigureAwait(false);

        //        if (response.IsSuccessStatusCode)
        //        {
        //            var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false); ;
        //            if (!string.IsNullOrWhiteSpace(json) && json != "ERROR")
        //            {
        //                result = new DistanceToExistingEv(JObject.Parse(json));
        //                if (result.value < 550)//less than 500 meters
        //                {
        //                    isNearExistingEV = true;
        //                    return isNearExistingEV;
        //                }
        //            }
        //        }
        //    }
        //    return isNearExistingEV;
        //}

    }
}
