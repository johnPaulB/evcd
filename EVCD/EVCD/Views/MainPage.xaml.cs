﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVCD.ViewModels;
using Xamarin.Forms;

namespace EVCD
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = _MainPageViewModel;

        }

        internal MainViewModel _MainPageViewModel { get; set; } = new MainViewModel();

        void TapGestureRecognizer_Tapped(System.Object sender, System.EventArgs e)
        {
        }
    }
}

